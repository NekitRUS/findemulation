﻿using System;
using System.IO;
namespace ConsoleApplication1
{
    class Program
    {
        static void help()
        {
            Console.Clear();
            Console.WriteLine("Формат команды: find [параметры] поисковый_запрос.\nРеализованы следующие параметры:\nПоиск файла для конкретного пользователя: -user имя_пользователя\nПоиск файла с учетом размера: -size ?размер, где ? - знак >, < или =");
            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
            Console.Clear();
        }
        static void Main()
        {
            string command = "";
            bool user, size;
            int findSize = 0;
            int matchCounter;
            StreamReader sr;
            string findUser= "";
            char sign = ' ';
            while (true)
            {
                Console.Clear();
                user = false;
                size = false;
                matchCounter = 0;
                Console.WriteLine("Введите команду. Для вызова справки введите help, для выхода - 0.");
                command = Console.ReadLine();
                string[] str = command.Split(' ');
                if (str[0] == "0")
                    break;
                if (command == "help")
                    help();
                if ((command == "") || (str[0] != "find"))
                    continue;
                try
                {
                    sr = new StreamReader("input.txt");
                    for (int i = 1; i < str.Length - 1; i++)
                    {
                        if (str[i] == "-user")
                        {
                            user = true;
                            findUser = str[i + 1];
                        }
                        if (str[i] == "-size")
                        {
                            size = true;
                            sign = char.Parse(str[i + 1].Substring(0, 1));
                            findSize = int.Parse(str[i + 1].Substring(1));
                        }
                    }
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] commands = line.Split(' ');
                        if (user && size)
                        {
                            if ((findUser == commands[3]) && (str[str.Length - 1] == commands[commands.Length - 1]) && (sign == '=') && (findSize == int.Parse(commands[4])))
                            {
                                matchCounter++;
                                Console.WriteLine(line);
                            }
                            if ((findUser == commands[3]) && (str[str.Length - 1] == commands[commands.Length - 1]) && (sign == '>') && (findSize < int.Parse(commands[4])))
                            {
                                matchCounter++;
                                Console.WriteLine(line);
                            }
                            if ((findUser == commands[3]) && (str[str.Length - 1] == commands[commands.Length - 1]) && (sign == '<') && (findSize > int.Parse(commands[4])))
                            {
                                matchCounter++;
                                Console.WriteLine(line);
                            }
                        }
                        else
                        {
                            if (user)
                            {
                                if ((findUser == commands[3]) && (str[str.Length - 1] == commands[commands.Length - 1]))
                                {
                                    matchCounter++;
                                    Console.WriteLine(line);
                                }
                            }
                            else
                            {
                                if (size)
                                {
                                    if ((str[str.Length - 1] == commands[commands.Length - 1]) && (sign == '=') && (findSize == int.Parse(commands[4])))
                                    {
                                        matchCounter++;
                                        Console.WriteLine(line);
                                    }
                                    if ((str[str.Length - 1] == commands[commands.Length - 1]) && (sign == '>') && (findSize < int.Parse(commands[4])))
                                    {
                                        matchCounter++;
                                        Console.WriteLine(line);
                                    }
                                    if ((str[str.Length - 1] == commands[commands.Length - 1]) && (sign == '<') && (findSize > int.Parse(commands[4])))
                                    {
                                        matchCounter++;
                                        Console.WriteLine(line);
                                    }
                                }
                                else
                                {
                                    if (str[str.Length - 1] == commands[commands.Length - 1])
                                    {
                                        matchCounter++;
                                        Console.WriteLine(line);
                                    }
                                }
                            }
                        }
                    }
                    sr.Close();
                }
                catch (FileNotFoundException)
                {
                    Console.WriteLine("Файл не найден!\nПоместите файл в папку с программой и перезапустите её.");
                    Console.ReadKey();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Ошибка!\n{0}", ex.Message);
                    Console.ReadKey();
                    continue;
                }
                if (matchCounter == 0)
                    Console.WriteLine("По запросу \"{0}\" ничего не найдено!", str[str.Length - 1]);
                Console.ReadKey();
            }
        }
    }
}
